FROM nginx:alpine
COPY index.html /usr/share/nginx/html
VOLUME [ "/home/yevhenii/Minikube&NGINX" ]
CMD ["nginx", "-g", "daemon off;"]